FROM theyoctojester/yocto-devcontainer:latest

ADD esdk.sh $HOME/esdk.sh
RUN $HOME/esdk.sh -y && \
    rm $HOME/esdk.sh && \
    ln -s ~/poky_sdk/environment-setup-* .bashrc.d/esdk-env-setup
